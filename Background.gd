extends Node2D

var sprite_size

func _ready():
	sprite_size = $Sprite.region_rect

func _process(delta):
	set_position(get_position() + Vector2(-1, 0))
	if (abs(get_position().x) >= 720):
		set_position(Vector2(0, 0))
