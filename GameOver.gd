extends Control

func _ready():
	$DistanceScore.text = String(stepify(Global.distance, 1))

func _on_Button_pressed():
	get_tree().change_scene("res://MainMenu.tscn")
