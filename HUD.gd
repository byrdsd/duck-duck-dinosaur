extends Control

func update_distance_score(distance):
	var distance_text = "Distance Traveled: %s"
	$DistanceScore.set_text(distance_text % distance)

