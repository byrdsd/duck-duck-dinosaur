extends Node

export (Array, PackedScene) var Enemies
var screensize
var distance_traveled = 0
var hud
var score = true
var spawn_counter = 0

func _ready():
	screensize = get_viewport().size
	_start_level()
	hud = get_node("CanvasLayer/HUD")

func _start_level():
	$EnemyTimer.start()
	$DistanceTimer.start()
	randomize()

func _spawn_enemies():
	var difficulty_multiplier = Global.distance * .0002
	var enemy_count = 1 + (randf() * difficulty_multiplier)
	for i in floor(enemy_count):
		_spawn_enemy()

func _spawn_enemy():
	Enemies.shuffle()
	var enemy = Enemies[0].instance()
	while enemy.spawn_threshold() > spawn_counter || enemy.distance_threshold() > distance_traveled:
		enemy.queue_free()
		Enemies.shuffle()
		enemy = Enemies[0].instance()
	if enemy.spawn_threshold() > 0:
		spawn_counter = 0
	add_child(enemy)
	spawn_counter += 1

func _on_EnemyTimer_timeout():
	$EnemyTimer.set_wait_time(rand_range(0.5, 1.5))
	_spawn_enemies()

func _on_DistanceTimer_timeout():
	if !score:
		return
	distance_traveled += 1
	Global.distance = distance_traveled
	hud.update_distance_score(distance_traveled)

func _on_Player_game_over():
	get_tree().change_scene("res://GameOver.tscn")


func _on_Player_player_death():
	score = false
