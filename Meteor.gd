extends Area2D

var velocity
var screensize

func spawn_threshold():
	return 2

func is_enemy():
	return true

func distance_threshold():
	return 3500

func _ready():
	screensize = get_viewport_rect().size
	set_position(_get_spawn_point())
	velocity = Vector2(-rand_range(100, 250), rand_range(100, 250))

func _get_spawn_point():
	return Vector2(rand_range(3, screensize.x / 50) * 50, 0)

func _physics_process(delta):
	set_position(get_position() + velocity * delta)

func _on_VisibilityNotifier2D_viewport_exited(viewport):
	queue_free()
