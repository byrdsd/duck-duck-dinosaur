extends Area2D

signal game_over
signal player_death
var velocity = 0
var max_velocity = get_gravity() * 4
var min_velocity = max_velocity * -1
var screensize
var _dead = false

func is_enemy():
	return false

func _ready():
	screensize = get_viewport_rect().size
	
func _physics_process(delta):
	if _dead:
		_spin_out(delta)
		_check_boundaries()
		_fall()
		set_position(get_position() + Vector2(0, velocity) * delta)

func _input(event):
	if _dead: return
	if event is InputEventScreenTouch && event.is_pressed():
		_flap()
	if event is InputEventKey && event.is_pressed():
		_flap()

func _spin_out(delta):
	set_rotation(get_rotation() + 0.6)
	set_position(get_position() + Vector2(75, 0) * delta)
			
func _check_boundaries():
	if get_position().y > screensize.y:
		if _dead:
			emit_signal("game_over")
		_flap()
	if get_position().y < 0 && velocity < 0:
		if _dead:
			return
		velocity = 0

func _flap():
	$AnimationPlayer.play("Flap")
	$AnimationPlayer.queue("Idle")
	velocity += -350
	if velocity < min_velocity:
		velocity = min_velocity

func _fall():
	if velocity < max_velocity:
		velocity += 9.8
	else:
		velocity = max_velocity

func _die():
	emit_signal ("player_death")
	velocity = -150
	max_velocity = get_gravity() * 10
	_dead = true

func _on_Player_area_entered(area):
	if area.is_enemy():
		_handle_enemy_collision(area)

func _handle_enemy_collision(enemy):
	if _dead:
		return
	_die()

func _on_Sprite_animation_finished():
	$Sprite.play("Idle")
