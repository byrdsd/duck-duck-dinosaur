extends Area2D

var size = {
    'current': 0,
    'max': 3,
    'min': 5
}
var speed = {
    'current': 0,
    'max': 225,
    'min': 150
}
var vertical_speed
var velocity = Vector2()
var screensize

func die():
    queue_free()

func is_enemy():
    return true

func spawn_threshold():
	return 0

func distance_threshold():
	return 0

func _ready():
    randomize()
    _initialize_config()
    screensize = get_viewport_rect().size
    position = _get_spawn_point()

func _initialize_config():
	size.current = rand_range(size.min, size.max)
	speed.current = rand_range(speed.min, speed.max)
	vertical_speed = -speed.current / 2
	_resize()

func _get_spawn_point():
	return Vector2(screensize.x + 100, rand_range(1, screensize.y / 50) * 50)

func _resize():
	var new_size = size.current
	set_scale(Vector2(new_size, new_size))

func _physics_process(delta):
	_handle_vertical_speed()
	_set_velocity()
	_move(delta)
	_check_boundaries()

func _handle_vertical_speed():
	vertical_speed += 5
	if vertical_speed >= speed.current / rand_range(1, 3):
		vertical_speed = -speed.current / rand_range(1, 3)

func _set_velocity():
	velocity = Vector2(-speed.current, vertical_speed)

func _move(delta):
	set_position(get_position() + velocity * delta)

func _check_boundaries():
	if get_position().y > screensize.y - 50:
		vertical_speed = -speed.current / 2
	if get_position().y < 50:
		vertical_speed = 0

func _on_Visibility_viewport_exited(viewport):
    die()
