extends Area2D

var velocity = Vector2(-125, -700)
var screensize

func is_enemy():
	return true

func spawn_threshold():
	return 3

func distance_threshold():
	return 2000

func _ready():
	screensize = get_viewport_rect().size
	position = _get_spawn_point()

func _get_spawn_point():
	return Vector2(screensize.x * .75, screensize.y + 250)

func _physics_process(delta):
	velocity = velocity + Vector2(0, 10)
	_move(delta)
	
func _move(delta):
	set_position(get_position() + velocity * delta)

func _on_VisibilityNotifier2D_viewport_exited(viewport):
	queue_free()
